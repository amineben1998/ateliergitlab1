FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

ENV PORT 5000
EXPOSE $PORT
ENTRYPOINT [ "sh", "-c", "java -Dserver.port=${PORT} -jar /usr/src/app/target/my-app.jar" ]
