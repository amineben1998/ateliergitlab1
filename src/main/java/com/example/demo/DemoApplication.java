package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;


@SpringBootApplication
@RestController
public class DemoApplication {
    String nom = "saif";
    int age = 30; // variable non utilisée

    @GetMapping("/")
    String home() {
        return "Spring is here!";
    }

    public static void main(String[] args) {

        String data ="donnees";
        SpringApplication.run(DemoApplication.class, args);
    }
}
